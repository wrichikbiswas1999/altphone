package uk.ac.liverpool.altphone.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.audiofx.Equalizer
import android.os.Build
import android.util.Log
import androidx.collection.SparseArrayCompat
import androidx.collection.contains
import androidx.collection.forEach
import androidx.collection.keyIterator
import androidx.collection.set
import androidx.core.app.NotificationCompat
import androidx.core.content.getSystemService
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.MainActivity
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.alternate.AlternateSettingsManager
import uk.ac.liverpool.altphone.measurements.BandsInfoManager
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.Measurement
import uk.ac.liverpool.altphone.measurements.MeasurementsDB
import uk.ac.liverpool.altphone.measurements.toInterpolator
import uk.ac.liverpool.altphone.measurements.toRoom
import uk.ac.liverpool.altphone.protos.BandsInfo
import uk.ac.liverpool.altphone.sessions.SessionsDB
import uk.ac.liverpool.altphone.single.SingleSettingsManager
import uk.ac.liverpool.altphone.utils.deserialize
import uk.ac.liverpool.altphone.utils.mapIfNotNull

class AltPhoneService : Service(), Observer<List<Int>> {

    companion object {
        private val FG_NOTIFCATION_ID = 10
        private val SERVICE_NAME = "Service Notification"
        private val SERVICE_DESCRIPTION = "Chnannel used to notifying that the app is running"
    }

    private val deserFunction: String.() -> Headphone? = {
        toByteArray().deserialize()
    }
    private var mode = Mode.SINGLE
    private var singleSource: Headphone? = null
    lateinit var singleBands: DoubleArray
    private var altSource: Headphone? = null
    private var altTarget: Headphone? = null
    lateinit var altBands: DoubleArray

    private var sparseArray: SparseArrayCompat<Equalizer> = SparseArrayCompat()
    lateinit var sessionsLiveData: LiveData<List<Int>>

    lateinit var builder: NotificationCompat.Builder
    lateinit var bandsInfo: BandsInfo

    override fun onBind(intent: Intent) = object : IAltPhoneServiceInterface.Stub() {
        override fun setMode(mode: String?) : String {
            Mode.valueOf(mode!!).apply {
                Log.i(javaClass.name, "Switching modes: ${this@AltPhoneService.mode to this}")
                this@AltPhoneService.mode = this
            }
            updateEQs()
            synchronized(sparseArray) {
                sparseArray.forEach { key, value ->
                    println(key to value.properties)
                }
            }
            updateNotification()
            return bands
        }

        override fun updateSettings(): String {
            reloadSettings()
            updateBands()
            println(singleBands.contentToString())
            println(altBands.contentToString())
            updateEQs()
            synchronized(sparseArray) {
                sparseArray.forEach { key, value ->
                    println(key to value.properties)
                }
            }
            updateNotification()
            return bands
        }

        override fun stop() {
            Log.i(javaClass.name, "Binding Stop")
            stopForeground(true)
            Log.i(javaClass.name, "Binding StopFG")
            stopSelf()
            Log.i(javaClass.name, "Binding StopSelf")
        }

        override fun getMode(): String = this@AltPhoneService.mode.name

        override fun getBands(): String = "Requested Bands (mB):\n" +
                "${(if (this@AltPhoneService.mode == Mode.SINGLE) singleBands else altBands).map { (it * 100).toShort() }}\n" +
                "Actual Bands (mB):\n" +
                "${if (sparseArray.isEmpty) "No EQ Applied" else sparseArray.valueAt(0).run {
                    (0 until numberOfBands)
                        .map {
                            getBandLevel(it.toShort())
                        }
                        .toList()
                }}"
    }

    private fun updateEQs() {
        synchronized(sparseArray) {
            sparseArray.forEach { _, value ->
                value.enabled = false
                (0 until value.numberOfBands).forEach {
                    value.setBandLevel(it.toShort(), ((if (mode == Mode.SINGLE) singleBands else altBands)[it] * 100.0).toShort())
                }
                value.enabled = true
            }
        }
    }

    override fun onDestroy() {
        sessionsLiveData.removeObserver(this)
        synchronized(sparseArray) {
            sparseArray.forEach { key, value ->
                val props = value.properties
                value.enabled = false
                value.release()
                sparseArray.remove(key)
                Log.i(javaClass.simpleName, "Removed for Session ${key to props}")
            }
        }
        Log.i(javaClass.name, "Destroyed")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Log.i(javaClass.name, "Started")
        reloadSettings()
        updateBands()
        sessionsLiveData = SessionsDB.getInstance(this)
            .sessionDao()
            .getOpenSessionsLive()
            .also { it.observeForever(this) }
        startForeground(
            FG_NOTIFCATION_ID,
            getUpdatedNotification()
        )
        return START_STICKY
    }

    override fun onChanged(list: List<Int>) {
        synchronized(sparseArray) {
            if (0 in list) {
                sparseArray.keyIterator()
                    .asSequence()
                    .filter {
                        it != 0
                    }
                    .toList()
                    .forEach {
                        with(sparseArray[it]!!) {
                            enabled = false
                            release()

                        }
                        sparseArray.remove(it)
                    }
                if (0 !in sparseArray) {
                    sparseArray[0] = Equalizer(0, 0)
                }
            } else {
                val keys = sparseArray.keyIterator().asSequence().toList()
                list.forEach {
                    if (it !in keys) {
                        sparseArray[it] = Equalizer(0, it)
                    }
                }
                keys.forEach {
                    if (it !in list) {
                        val eq = sparseArray[it]!!
                        val props = eq.properties
                        eq.enabled = false
                        eq.release()
                        sparseArray.remove(it)
                        Log.i(javaClass.simpleName, "Removed for Session $it, $props")
                    }
                }
            }
            updateEQs()
        }
    }

    override fun onCreate() {
        super.onCreate()
        Log.v(javaClass.simpleName, "OnCreate")
        createChannel()
        builder = NotificationCompat.Builder(this, packageName)
            .setAutoCancel(false)
            .setColorized(true)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setSmallIcon(packageManager.getApplicationInfo(packageName, 0).icon)
            .setContentTitle("AltPhone")
            .setContentText("Starting Service")
            .setContentIntent(
                PendingIntent.getActivity(
                    this,
                    FG_NOTIFCATION_ID,
                    Intent(this, MainActivity::class.java),
                    0
                )
            )
        startForeground(FG_NOTIFCATION_ID, builder.build())
        with(BandsInfoManager.getSettings(this).data) {
            runBlocking {
                first()
                    .also { bandsInfo = it }
                    .info
                    .apply {
                        singleBands = DoubleArray(bandsCount)
                        altBands = DoubleArray(bandsCount)
                    }
            }
        }
    }

    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService<NotificationManager>()!!
            if (SERVICE_NAME !in notificationManager.notificationChannels.map(NotificationChannel::getName)) {
                NotificationChannel(
                    packageName,
                    SERVICE_NAME,
                    NotificationManager.IMPORTANCE_LOW
                ).run {
                    description = SERVICE_DESCRIPTION
                    notificationManager.createNotificationChannel(this)
                }
            }
        }
    }

    private fun updateNotification() = getSystemService<NotificationManager>()!!.apply {
        notify(
            FG_NOTIFCATION_ID,
            getUpdatedNotification()
        )
    }

    private fun getUpdatedNotification() = builder.setContentText("Service Started: ${mode.toHumanString()} Mode")
        .setStyle(makeSettingsStyle())
        .build()

    private fun makeSettingsStyle() =
        NotificationCompat.BigTextStyle().setBigContentTitle("AltPhone")
            .setSummaryText("AltPhone Service State").bigText("Normalisation: ${singleSource.mapIfNotNull {
                "$name targeting $target by $outlet"
            } ?: "Unselected"}\n" +
                    "Alternate Source: ${altSource?.name ?: "Unselected"}\n" +
                    "Alternate Target: ${altTarget?.name ?: "Unselected"}\n" +
                    "Mode: ${mode.toHumanString()}")

    private fun reloadSettings() {
        runBlocking {
            SingleSettingsManager.getSettings(this@AltPhoneService)
                .data
                .first()
                .apply {
                    if (hasHeadphone()) {
                        singleSource = headphone.toRoom()
                    }
                }
            AlternateSettingsManager.getSettings(this@AltPhoneService)
                .data
                .first()
                .apply {
                    if (hasSource()) {
                        altSource = source.toRoom()
                    }
                    if (hasTarget()) {
                        altTarget = target.toRoom()
                    }
                }
        }
    }

    private fun updateBands() {
        val mDao = MeasurementsDB.getInstance(this)
            .measurementDao()
        singleBands = if (singleSource == null) {
            DoubleArray(bandsInfo.info.bandsCount)
        } else {
            val temp = mDao
                .getAllByHeadphone(singleSource!!.id!!)
                .toInterpolator()
                .getBandFreqs(bandsInfo)
                .toDoubleArray()
            val range = temp.minOrNull()!! to temp.maxOrNull()!!
            var diff = 0.0
            if (range.first < bandsInfo.info.minLevel) {
                diff = bandsInfo.info.minLevel - range.first
            } else if (range.second > bandsInfo.info.maxLevel) {
                diff = bandsInfo.info.maxLevel - range.second
            }
            temp.map { it + diff }
                .toDoubleArray()
        }
        altBands = if (altSource == null || altTarget == null) {
            DoubleArray(bandsInfo.info.bandsCount)
        } else {
            val src = mDao.getAllByHeadphone(altSource!!.id!!)
                .associate { it.frequency to it.level }
            val tgt =  mDao.getAllByHeadphone(altTarget!!.id!!)
                .associate { it.frequency to it.level }
            val temp = src.mapValues {
                it.value - tgt[it.key]!!
            }
                .entries
                .map { Measurement(intArrayOf(altSource!!.id!!, altTarget!!.id!!).hashCode(), it.key, it.value) }
                .sortedBy { it.frequency }
                .toInterpolator()
                .getBandFreqs(bandsInfo)
                .toDoubleArray()
            val range = temp.minOrNull()!! to temp.maxOrNull()!!
            var diff = 0.0
            if (range.first < bandsInfo.info.minLevel) {
                diff = bandsInfo.info.minLevel - range.first
            } else if (range.second > bandsInfo.info.maxLevel) {
                diff = bandsInfo.info.maxLevel - range.second
            }
            temp.map { it + diff }
                .toDoubleArray()
        }
    }
}

enum class Mode {
    SINGLE, ALTERNATE;

    companion object {
        fun getModeByID(id: Int): Mode? = when (id) {
            R.id.single_mode_mi -> SINGLE
            R.id.alternate_mode_mi -> ALTERNATE
            else -> null
        }
    }

    fun getMenuID(): Int = when (this) {
        SINGLE -> R.id.single_mode_mi
        ALTERNATE -> R.id.alternate_mode_mi
    }

    operator fun not(): Mode = values()[1 - ordinal]

    fun toHumanString(): String = with(toString()) {
        "${this[0]}${this.substring(1).toLowerCase()}"
    }
}
