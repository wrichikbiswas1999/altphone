package uk.ac.liverpool.altphone

import android.Manifest
import android.app.ActivityManager
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.media.audiofx.Equalizer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.PermissionChecker
import androidx.core.content.getSystemService
import androidx.core.view.MenuItemCompat
import androidx.core.view.children
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import java.util.stream.Collectors
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.databinding.ActivityMainBinding
import uk.ac.liverpool.altphone.databinding.LayoutNoEqBinding
import uk.ac.liverpool.altphone.measurements.BandsInfoManager
import uk.ac.liverpool.altphone.protos.BandsInfo
import uk.ac.liverpool.altphone.protos.EqBand
import uk.ac.liverpool.altphone.protos.EqInfo
import uk.ac.liverpool.altphone.service.AltPhoneService
import uk.ac.liverpool.altphone.service.IAltPhoneServiceInterface
import uk.ac.liverpool.altphone.service.Mode
import uk.ac.liverpool.altphone.sessions.LegacySessionSettingsManager
import uk.ac.liverpool.altphone.utils.HAS_EQ

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val PERMISSION_REQUEST_OPTS = 1

    private val REQUEST_IGNORE_INTENT by lazy {
        Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
            .setData(Uri.parse("package:$packageName"))
    }

    private var binder: IAltPhoneServiceInterface? = null

    private var connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            binder = IAltPhoneServiceInterface.Stub.asInterface(service!!)
            Log.i(this::class.java.name, "$name connection: $service")
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            binder = null
            Log.i(this::class.java.name, "$name D/C")
        }

        override fun onBindingDied(name: ComponentName?) {
            super.onBindingDied(name)
            Log.i(this::class.java.name, "$name died")
        }

        override fun onNullBinding(name: ComponentName?) {
            super.onNullBinding(name)
            Log.i(this::class.java.name, "$name null")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (HAS_EQ) {
            val binding = ActivityMainBinding.inflate(layoutInflater)
            setContentView(binding.root)
            val adapter = MainActivityAdapter(this, binding)
            with(ComponentName(this, AltPhoneService::class.java)) {
                if (this in getSystemService<ActivityManager>()!!.getRunningServices(Int.MAX_VALUE)
                        .map(ActivityManager.RunningServiceInfo::service)
                ) {
                    bindService(Intent().setComponent(this), connection, 0)
                }
            }
            val store = BandsInfoManager.getSettings(this)
            val bands = runBlocking { store.data.first() }
            if (!bands.init) {
                runBlocking {
                    store.updateData {
                        val builder = BandsInfo.newBuilder()
                        builder.init = true
                        val eq = Equalizer(1, 0)
                        val infoBuilder = EqInfo.newBuilder()
                        with(eq.bandLevelRange) {
                            infoBuilder.maxLevel = this[1].toDouble() / 100
                            infoBuilder.minLevel = this[0].toDouble() / 100
                        }
                        val numBands = eq.numberOfBands
                        (0 until numBands)
                            .map(Int::toShort)
                            .forEach {
                                val bandBuilder = EqBand.newBuilder()
                                with(eq.getBandFreqRange(it)) {
                                    bandBuilder.minFreq = this[0]
                                    bandBuilder.minWork = this[0].toDouble() / 1000
                                    bandBuilder.maxFreq = this[1]
                                    bandBuilder.maxWork =
                                        if (this[1] < this[0] && numBands - it == 1) {
                                            20000.toDouble()
                                        } else {
                                            this[1].toDouble() / 1000
                                        }
                                }
                                bandBuilder.centreFreq = eq.getCenterFreq(it)
                                bandBuilder.centreWork = eq.getCenterFreq(it).toDouble() / 1000
                                infoBuilder.addBands(bandBuilder.build())
                            }
                        builder.info = infoBuilder.build()
                        eq.release()
                        return@updateData builder.build()
                            .also {
                                Log.i("Bands", it.toString())
                            }
                    }
                }
            }
        } else {
            val binding = LayoutNoEqBinding.inflate(layoutInflater)
            setContentView(binding.root)
            binding.noEqButton.setOnClickListener(this)
        }
    }

    override fun onClick(v: View?) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            .setData(Uri.fromParts("package", packageName, null))
        startActivity(Intent.createChooser(intent, "Uninstall App"))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_main, menu)
        return HAS_EQ
    }

    var menuItems = emptyList<MenuItemTexts>()

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu!!.findItem(R.id.disable_power_savings_mi).isVisible = shouldShowDisable()
        with(menu.findItem(R.id.legacy_mode_mi)) {
            if (LegacySessionSettingsManager.needForceLegacy(this@MainActivity) || !runBlocking {
                    LegacySessionSettingsManager.getSettings(
                        this@MainActivity
                    ).data.first()
                }.legacyAvailable) {
                isVisible = false
            } else {
                isChecked =
                    runBlocking {
                        LegacySessionSettingsManager.getSettings(this@MainActivity).data.first()
                            .also {
                                Log.i(it.javaClass.name, it.toByteString().toString())
                            }
                    }.enableLegacy
            }
        }
        with(
            ComponentName(
                this,
                AltPhoneService::class.java
            ) in getSystemService<ActivityManager>()!!.getRunningServices(Int.MAX_VALUE)
                .map(ActivityManager.RunningServiceInfo::service)
        ) {
            menu.findItem(R.id.start_service_mi).isVisible = !this
            menu.findItem(R.id.stop_service_mi).isVisible = this
            menu.findItem(R.id.update_service_mi).isVisible = this
            menu.findItem(R.id.service_mode_mi).isVisible = this
            menu.findItem(R.id.get_bands_mi).isVisible = this
            if (this) {
                menu.findItem(Mode.valueOf(binder!!.mode).getMenuID()).isChecked = true
            }
        }
        with(HAS_EQ) {
            menu.findItem(R.id.eq_info_mi).isVisible = this
        }
        menuItems = (0 until menu.size())
            .map(menu::getItem)
            .filter(MenuItem::isVisible)
            .flatMap { it: MenuItem ->
                val temp : MutableList<MenuItem> = mutableListOf(it)
                if (it.hasSubMenu()) {
                    it.subMenu
                        .children
                        .forEach(temp::add)
                }
                return@flatMap temp
            }
            .map(MenuItem::getTexts)
            .filter { it: MenuItemTexts ->
                it.tooltipText != null
            }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        print(MenuItemCompat.getTooltipText(item))
        when (item.itemId) {
            R.id.disable_power_savings_mi -> disableOptimizations()
            R.id.legacy_mode_mi -> (!item.isChecked).apply {
                println(this)
                Log.e(this@MainActivity::class.java.name, item.toString())
                LegacySessionSettingsManager.setLegacyMode(this@MainActivity, this)
                item.isChecked = this
            }
            R.id.start_service_mi -> {
                Intent(this, AltPhoneService::class.java).apply {
                    startService(this)
                    bindService(this, connection, 0)
                }
            }
            R.id.stop_service_mi -> {
                Intent(this, AltPhoneService::class.java).apply {
                    binder!!.stop()
                    stopService(this)
                    unbindService(connection)
                    binder = null
                }
            }
            R.id.update_service_mi -> {
                showResults("Settings Reloaded", binder!!.updateSettings())
            }
            R.id.get_bands_mi -> {
                showResDialog("Bands", binder!!.bands)
            }
            R.id.single_mode_mi, R.id.alternate_mode_mi -> {
                val selected = Mode.getModeByID(item.itemId)!!
                val status = binder!!.setMode(selected.name)
                item.isChecked = true
                println(!selected)
                showResults("Mode Updated Sucessfully", status)
            }
            R.id.eq_info_mi -> {
                with(Equalizer(0, 1)) {
                    MaterialAlertDialogBuilder(this@MainActivity)
                        .setTitle("Equalizer Engine Information")
                        .setMessage(this.descriptor.run {
                            "Name: $name\nImplementor: $implementor\nConnection Method: $connectMode\nID: $uuid\nType ID: $type"
                        })
                        .setNegativeButton("Dismiss", null)
                        .setNeutralButton("Parameters") { _, _ ->
                            val bands =
                                runBlocking { BandsInfoManager.getSettings(this@MainActivity).data.first() }
                            MaterialAlertDialogBuilder(this@MainActivity)
                                .setTitle("Equalizer Band Parameter Information")
                                .setMessage(with(bands.info) {
                                    (0 until bandsCount)
                                        .map {
                                            getBands(it).run {
                                                "Band ${it + 1}: $minWork - ${maxWork}, Centre: $centreWork Hz\n"
                                            }
                                        }
                                        .stream()
                                        .collect(Collectors.joining())
                                        .plus(
                                            "Gain Range: ${minLevel.toInt().toShort() * 100} - ${
                                                maxLevel.toInt().toShort() * 100
                                            } mB"
                                        )
                                })
                                .setCancelable(false)
                                .setNegativeButton("Dismiss", null)
                                .show()
                        }
                        .setCancelable(false)
                        .show()
                    release()
                }
            }
            R.id.info_mi -> {
                synchronized(menuItems) {
                    MaterialAlertDialogBuilder(this)
                        .setTitle("Info")
                        .setItems(menuItems.map(MenuItemTexts::text).toTypedArray()) { _, which ->
                            showResDialog(menuItems[which].text.toString(), menuItems[which].tooltipText.toString())
                        }
                        .setNegativeButton("Dismiss", null)
                        .show()
                }
            }
            else -> {
                Log.e(this@MainActivity::class.java.name, item.toString())
            }
        }
        return true
    }


    private fun shouldShowDisable(): Boolean =
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && !getSystemService<PowerManager>()!!.isIgnoringBatteryOptimizations(
            packageName
        )

    private fun disableOptimizations() {
        if (PermissionChecker.checkSelfPermission(
                this,
                Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
            ) != PermissionChecker.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS),
                    PERMISSION_REQUEST_OPTS
                )
            }
        } else {
            startActivity(REQUEST_IGNORE_INTENT)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults[0] == PermissionChecker.PERMISSION_GRANTED) {
            startActivity(REQUEST_IGNORE_INTENT)
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun showResults(sbText: String, adMsg: String) {
        val bar = Snackbar.make(
            findViewById(android.R.id.content),
            sbText,
            Snackbar.LENGTH_LONG
        )
        if (adMsg.isNotEmpty()) {
            bar.setAction("Details") {
                showResDialog(adMsg = adMsg)
            }
        }
        bar.show()
    }

    private fun showResDialog(title: String= "Details", adMsg: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle(title)
            .setPositiveButton("Dismiss", null)
            .setMessage(adMsg)
            .setCancelable(false)
            .show()
    }
}

fun MenuItem.getTexts() = MenuItemTexts(itemId, title, MenuItemCompat.getTooltipText(this))

data class MenuItemTexts(val id: Int, val text: CharSequence, val tooltipText : CharSequence?)