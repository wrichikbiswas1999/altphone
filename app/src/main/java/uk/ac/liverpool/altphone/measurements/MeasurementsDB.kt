package uk.ac.liverpool.altphone.measurements

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.RoomWarnings
import uk.ac.liverpool.altphone.protos.BandsInfo
import java.io.Serializable
import java.util.regex.Pattern

@Database(
    entities = [Headphone::class, Measurement::class, Init::class],
    version = 4
)
abstract class MeasurementsDB : RoomDatabase() {

    abstract fun headphoneDao(): HeadphoneDao

    abstract fun initDao(): InitDao

    abstract fun measurementDao(): MeasurementDao

    companion object {
        private var instance: MeasurementsDB? = null

        fun getInstance(context: Context): MeasurementsDB = instance ?: synchronized(this) {
            instance ?: Room.databaseBuilder(context, MeasurementsDB::class.java, "measurements")
                .enableMultiInstanceInvalidation()
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
                .also { instance = it }
        }
    }

}

@Entity
data class Headphone(
    @ColumnInfo @PrimaryKey(autoGenerate = true) var id: Int? = null,
    @ColumnInfo var name: String? = "",
    @ColumnInfo var outlet: String? = "",
    @ColumnInfo var target: String? = "",
    @ColumnInfo var measurements: String? = ""
) : Serializable {

    companion object {
        val pattern: Pattern by lazy { Pattern.compile("\"(.*?)\",\"(.*?)\",\"(.*?)\",\"(.*?)\"") }
    }

    constructor(line: String) : this(name = "", outlet = "", target = "", measurements = "") {
        val matcher = pattern.matcher(line)
        if (matcher.find()) {
            name = matcher.group(1)!!
            outlet = matcher.group(2)!!
            target = matcher.group(3)!!
            measurements = matcher.group(4)!!
        } else {
            Log.v(javaClass.simpleName, line)
        }
    }
}

fun Headphone.toProto(): uk.ac.liverpool.altphone.protos.Headphone =
    uk.ac.liverpool.altphone.protos.Headphone
        .newBuilder()
        .setId(id!!)
        .setName(name)
        .setOutlet(outlet)
        .setTarget(target)
        .setMeasurements(measurements)
        .build()

fun uk.ac.liverpool.altphone.protos.Headphone.toRoom() =
    Headphone(id, name, outlet, target, measurements)

@Dao
interface HeadphoneDao {

    @Query("select * from Headphone")
    fun getAll(): LiveData<List<Headphone>>

    @Query("select * from Headphone")
    fun getAllNow(): List<Headphone>

    @Query("select * from Headphone where name like '%'||:search||'%' or outlet like '%'||:search||'%' or target like '%'||:search||'%'")
    fun getAllByText(search: String): LiveData<List<Headphone>>

    @Query("select * from Headphone where name like '%'||:search||'%' group by name")
    fun searchAllByName(search: String): LiveData<List<Headphone>>

    @Query("select * from Headphone where name == :hName")
    fun getAllByNameNow(hName: String): List<Headphone>

    @Query("select * from Headphone where name == :hName")
    fun getAllByNameSingleNow(hName: String): List<Headphone>

    @Query("select distinct name from Headphone")
    fun getAllNames(): List<String>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHeadphone(headphone: Headphone): Long

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("select outlet, target from Headphone where name = :name")
    fun getOutletsAndTargets(name: String): List<Headphone>

    @Query("select * from Headphone as data where data.name in (select distinct name from Headphone names join (select outlet, target from Headphone where name = :name) as targets where names.target = targets.target and names.outlet = targets.outlet) and data.name like '%' || :search || '%' group by data.name")
    fun getAllApplicableTargets(name: String, search: String): LiveData<List<Headphone>>

    @Query("delete from Headphone")
    fun deleteAll()
}

@Entity(
    foreignKeys = [ForeignKey(
        entity = Headphone::class,
        childColumns = ["headphone"],
        parentColumns = ["id"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )], primaryKeys = ["headphone", "frequency"]
)
data class Measurement(
    @ColumnInfo var headphone: Int,
    @ColumnInfo var frequency: Int,
    @ColumnInfo var level: Double
) : Serializable {

    companion object {

        private val semiColonSplitter: Pattern by lazy { Pattern.compile(";") }
        private val spaceSplitter: Pattern by lazy { Pattern.compile(" ") }


        fun from(measurements: String, headphone: Int): Array<Measurement> {
            return measurements.substring(11)
                .split(semiColonSplitter)
                .map { it.trim() }
                .map { it.split(spaceSplitter) }
                .map { Measurement(headphone, it[0].toInt(), it[1].toDouble()) }
                .sortedBy { it.frequency }
                .toTypedArray()
        }
    }
}

class MeasurementInterpolator(private val map: Map<Double, Double>) : Map<Double, Double> {


    fun getMeasurementFor(double: Double): Double {
        if (double in map) {
            return map[double]!!
        } else {
            val belowFreq = map.keys.filter { it < double }.maxOrNull()
            val aboveFreq = map.keys.filter { it > double }.minOrNull()
            if (belowFreq == null) {
                return map[map.keys.minOrNull()!!]!!
            }
            if (aboveFreq == null) {
                return map[map.keys.maxOrNull()!!]!!
            }
            val scale = (double - belowFreq) / (aboveFreq - belowFreq)
            return map[belowFreq]!! + (scale * (map[aboveFreq]!! - map[belowFreq]!!))
        }
    }

    override val entries: Set<Map.Entry<Double, Double>>
        get() = map.entries
    override val keys: Set<Double>
        get() = map.keys
    override val size: Int
        get() = map.size
    override val values: Collection<Double>
        get() = map.values

    override fun containsKey(key: Double): Boolean = map.containsKey(key)

    override fun containsValue(value: Double): Boolean = map.containsValue(value)

    override fun get(key: Double): Double? = map[key]

    override fun isEmpty(): Boolean = map.isEmpty()

    fun getBandFreqs(bands: BandsInfo) : List<Double> {
        return bands.info.bandsList
            .map { band ->
                val arr = doubleArrayOf(band.minWork, band.maxWork)
                val keys = keys
                    .toMutableList()
                    .apply {
                        add(arr[0])
                        add(arr[1])
                    }
                    .filter { it >= arr[0] }
                    .filter { it <= arr[1] }
                    .sorted()
                    .distinct()
                    .toDoubleArray()
                (0 until (keys.size - 1))
                    .map { ind ->
                        val height = keys[ind + 1] - keys[ind]
                        val base =
                            (getMeasurementFor(keys[ind]) + getMeasurementFor(
                                keys[ind + 1]
                            )) / 2
                        base * height
                    }.sum() / (arr[1] - arr[0])
            }
    }
}

fun List<Measurement>.toInterpolator(): MeasurementInterpolator =
    this.sortedBy(Measurement::frequency)
        .associate { it.frequency.toDouble() to it.level }
        .run { MeasurementInterpolator(this) }

@Dao
interface MeasurementDao {

    @Query("select * from Measurement where headphone == :id")
    fun getAllByHeadphone(id: Int): List<Measurement>

    @Query("delete from Measurement")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(measurements: Array<Measurement>)
}

@Dao
interface InitDao {

    @Query("select commit_hash from Init limit 1")
    fun getHash(): String?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setCommitHash(init: Init)
}

@Entity
data class Init(@ColumnInfo @PrimaryKey var id: Int = 0, @ColumnInfo var commit_hash: String) :
    Serializable {

    constructor(commit_hash: String) : this(0, commit_hash)
}