package uk.ac.liverpool.altphone.sessions

import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.util.Log
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.createDataStore
import androidx.lifecycle.LiveData
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import java.io.InputStream
import java.io.OutputStream
import java.io.Serializable
import java.time.Instant
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.protos.LegacySessionSettings

@Database(entities = [Session::class], version = 2)
@TypeConverters(InstantConverters::class)
abstract class SessionsDB : RoomDatabase() {

    abstract fun sessionDao(): SessionDao

    companion object {
        private var instance: SessionsDB? = null

        fun getInstance(context: Context): SessionsDB = instance ?: synchronized(this) {
            instance ?: Room.databaseBuilder(context, SessionsDB::class.java, "sessions")
                .enableMultiInstanceInvalidation()
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
                .also { instance = it }
        }
    }
}

@Entity
data class Session(
    @ColumnInfo @PrimaryKey(autoGenerate = false) var id: Int? = null,
    @ColumnInfo var packageName: String? = "",
    @ColumnInfo var openTime: Instant?,
    @ColumnInfo var closeTime: Instant?,
    @ColumnInfo var open: Boolean
) : Serializable {

    companion object {
        val SESSION_0: Session by lazy {
            return@lazy Session(
                0,
                "android",
                null,
                null,
                true
            )
        }
    }
}

@Dao
interface SessionDao {

    @Query("select * from Session")
    fun getAllSessionsNow(): List<Session>

    @Query("select * from Session")
    fun getAllSessionsLive(): LiveData<List<Session>>

    @Query("select * from Session where id == :id")
    fun getSessionNow(id: Int): Session?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun putSession(session: Session)

    @Query("select id from Session where open == 1")
    fun getOpenSessionsLive(): LiveData<List<Int>>

    @Delete
    fun removeSession(session: Session)
}

class InstantConverters {

    @TypeConverter
    fun toLong(instant: Instant?): Long? = instant?.toEpochMilli()

    @TypeConverter
    fun toInstant(long: Long?): Instant? = long?.let(Instant::ofEpochMilli)
}

object LegacySessionSettingsManager : Serializer<LegacySessionSettings> {

    private var NEEDS_FORCE_LEGACY: Boolean? = null

    fun needForceLegacy(context: Context): Boolean = NEEDS_FORCE_LEGACY ?: synchronized(this) {
        NEEDS_FORCE_LEGACY ?: with(
            (context.packageManager.getPackageInfo(
                context.packageName,
                PackageManager.GET_RECEIVERS
            ).receivers ?: emptyArray()).map(
                ActivityInfo::name
            )
        ) {
            !(contains(SessionClosedReceiver::class.qualifiedName) && contains(SessionOpenedReceiver::class.qualifiedName))
        }
            .also { NEEDS_FORCE_LEGACY = it }
    }

    override val defaultValue: LegacySessionSettings = LegacySessionSettings.getDefaultInstance()

    override fun readFrom(input: InputStream): LegacySessionSettings =
        LegacySessionSettings.parseFrom(input)

    override fun writeTo(t: LegacySessionSettings, output: OutputStream) = t.writeTo(output)

    private fun handleCorruption(ex: CorruptionException): LegacySessionSettings {
        Log.e(LegacySessionSettingsManager::javaClass.name, ex.toString(), ex)
        return defaultValue
    }

    private var settings: DataStore<LegacySessionSettings>? = null

    fun getSettings(context: Context): DataStore<LegacySessionSettings> =
        settings ?: synchronized(this) {
            settings ?: context.createDataStore(
                fileName = "legacy_settings.pb",
                serializer = this,
                corruptionHandler = ReplaceFileCorruptionHandler(this::handleCorruption)
            ).also { settings = it }
        }

    fun setLegacyMode(context: Context, value: Boolean) {
        with(SessionsDB.getInstance(context).sessionDao()) {
            when (value) {
                true -> putSession(Session.SESSION_0)
                false -> removeSession(Session.SESSION_0)
            }
        }
        runBlocking {
            getSettings(context).updateData {
                it.toBuilder()
                    .setInit(true)
                    .setEnableLegacy(value)
                    .build()
            }

        }
    }
}

