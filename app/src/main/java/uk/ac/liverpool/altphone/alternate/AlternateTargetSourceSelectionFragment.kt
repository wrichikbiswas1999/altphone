package uk.ac.liverpool.altphone.alternate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.databinding.ActivityHeadphoneBinding
import uk.ac.liverpool.altphone.headphone.adapters.DiffObject
import uk.ac.liverpool.altphone.headphone.adapters.HeadphoneHolder
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.MeasurementsDB
import uk.ac.liverpool.altphone.measurements.toProto
import uk.ac.liverpool.altphone.utils.SimpleTextWatcher

class AlternateTargetSourceSelectionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.activity_headphone, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = ActivityHeadphoneBinding.bind(view)
        val recycler = binding.recycler
        val adapter: ListAdapter<Headphone, HeadphoneHolder> = AlternateSourceAdapter()
        recycler.adapter = adapter
        var data: LiveData<List<Headphone>>?
        data = MeasurementsDB.getInstance(requireContext())
            .headphoneDao()
            .searchAllByName("")
        val observer = Observer<List<Headphone>>(adapter::submitList)
        data.observe(viewLifecycleOwner, observer)
        val layout = LinearLayoutManager(requireContext())
        recycler.layoutManager = layout
        recycler.addItemDecoration(DividerItemDecoration(requireContext(), layout.orientation))
        val text = binding.nameBox
        text.addTextChangedListener(object : SimpleTextWatcher() {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                data!!.removeObservers(viewLifecycleOwner)
                data = null
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                data = MeasurementsDB.getInstance(requireContext())
                    .headphoneDao()
                    .searchAllByName(text.text.toString())
                data!!.observe(viewLifecycleOwner, observer)
            }
        })
    }
}

class AlternateSourceAdapter :
    ListAdapter<Headphone, HeadphoneHolder>(DiffObject) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeadphoneHolder =
        HeadphoneHolder.makeHolder(parent)

    override fun onBindViewHolder(holder: HeadphoneHolder, position: Int) {
        val headphone by lazy { getItem(position) }
        val button = holder.binding.button
        button.text = headphone.name!!
        button.setOnClickListener {
            runBlocking {
                AlternateSettingsManager.getSettings(button.context)
                    .updateData {
                        with(it.toBuilder()) {
                            source = headphone.toProto()
                            clearTarget()
                            build()
                        }
                    }
            }
            Navigation.findNavController(button)
                .navigate(
                    R.id.returnSource,
                    AlternateTargetFragmentArgs.Builder()
                        .setCheckSource(true)
                        .setSource(headphone)
                        .build()
                        .toBundle()
                )
        }
    }
}