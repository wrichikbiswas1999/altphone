package uk.ac.liverpool.altphone.sessions

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.audiofx.AudioEffect
import java.time.Instant

class SessionClosedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val now = Instant.now()
        val sessionId = intent.getIntExtra(AudioEffect.EXTRA_AUDIO_SESSION, -1)
        val name = intent.getStringExtra(AudioEffect.EXTRA_PACKAGE_NAME)
        with(SessionsDB.getInstance(context).sessionDao()) {
            this.putSession(
                with(
                    this.getSessionNow(sessionId) ?: Session(
                        sessionId,
                        name,
                        null,
                        now,
                        true
                    )
                ) session@{
                    closeTime = now
                    open = false
                    return@session this
                })
        }
    }
}