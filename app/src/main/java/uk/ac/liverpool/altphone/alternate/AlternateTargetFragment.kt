package uk.ac.liverpool.altphone.alternate

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.createDataStore
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import java.io.InputStream
import java.io.OutputStream
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.databinding.LayoutFragmentAlternateBinding
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.toRoom
import uk.ac.liverpool.altphone.protos.AlternateSettings
import uk.ac.liverpool.altphone.utils.ifNotNull

class AlternateTargetFragment : Fragment() {
    private val args: AlternateTargetFragmentArgs by navArgs()
    val source: Headphone? by lazy {
        if (args.checkSource) {
            args.source
        } else {
            with(runBlocking { AlternateSettingsManager.getSettings(requireContext()).data.first() }) {
                if (hasSource()) {
                    return@lazy source.toRoom()
                } else {
                    return@lazy null
                }
            }
        }
    }
    val target: Headphone? by lazy {
        if (args.checkTarget) {
            args.target
        } else {
            with(runBlocking { AlternateSettingsManager.getSettings(requireContext()).data.first() }) {
                if (hasTarget()) {
                    return@lazy target.toRoom()
                } else {
                    return@lazy null
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.layout_fragment_alternate, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = LayoutFragmentAlternateBinding.bind(view)
        source.ifNotNull {
            binding.sourceBox.text = name
            binding.targetButton.run {
                isEnabled = true
                setOnClickListener(
                    Navigation.createNavigateOnClickListener(
                        R.id.doubleTargetSelection,
                        AlternateTargetTargetSelectionFragmentArgs.Builder(this@ifNotNull)
                            .build()
                            .toBundle()
                    )
                )
            }
        }
        target.ifNotNull {
            binding.targetBox.text = name
        }
        binding.sourceButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.doubleSourceSelection))
    }
}

object AlternateSettingsManager : Serializer<AlternateSettings> {

    private var settings: DataStore<AlternateSettings>? = null

    override val defaultValue: AlternateSettings = AlternateSettings.getDefaultInstance()

    override fun readFrom(input: InputStream): AlternateSettings =
        AlternateSettings.parseFrom(input)

    override fun writeTo(t: AlternateSettings, output: OutputStream) = t.writeTo(output)

    private fun handleCorruption(ex: CorruptionException): AlternateSettings {
        Log.e(AlternateSettingsManager::javaClass.name, ex.toString(), ex)
        return AlternateSettings.newBuilder()
            .setInit(true)
            .build()
    }

    fun getSettings(context: Context): DataStore<AlternateSettings> =
        settings ?: synchronized(this) {
            settings ?: context.createDataStore(
                fileName = "alternate_settings.pb",
                serializer = AlternateSettingsManager,
                corruptionHandler = ReplaceFileCorruptionHandler(AlternateSettingsManager::handleCorruption)
            )
                .also { settings = it }

        }

}