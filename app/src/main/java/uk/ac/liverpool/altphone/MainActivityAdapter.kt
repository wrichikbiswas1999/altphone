package uk.ac.liverpool.altphone

import androidx.navigation.fragment.NavHostFragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import uk.ac.liverpool.altphone.databinding.ActivityMainBinding

class MainActivityAdapter(activity: MainActivity, binding: ActivityMainBinding) : FragmentStateAdapter(activity) {

    var tabs: TabLayoutMediator

    init {
        binding.viewPager.adapter = this
        tabs = TabLayoutMediator(binding.tabLayout, binding.viewPager, true, true) { tab, position ->
            tab.text = when (position) {
                0 -> "Normalisation"
                else -> "Alternate Headphone Equalisation"
            }
        }
        tabs.attach()
    }

    override fun getItemCount() = 2

    override fun createFragment(position: Int) = when (position) {
        0 -> NavHostFragment.create(R.navigation.nav_single)
        else -> NavHostFragment.create(R.navigation.nav_alternate)
    }
}