package uk.ac.liverpool.altphone.measurements

import android.app.ActivityManager
import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import androidx.core.content.getSystemService
import androidx.startup.Initializer
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.SequenceInputStream
import java.lang.reflect.Field
import java.util.Collections
import java.util.regex.Pattern
import java.util.zip.ZipInputStream
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.alternate.AlternateSettingsManager
import uk.ac.liverpool.altphone.service.AltPhoneService
import uk.ac.liverpool.altphone.service.IAltPhoneServiceInterface
import uk.ac.liverpool.altphone.single.SingleSettingsManager
import uk.ac.liverpool.altphone.utils.HAS_EQ

class MeasurementsDBInitializer : Initializer<MeasurementsDB> {

    companion object {
        val dataZipPattern: Pattern by lazy {
            return@lazy Pattern.compile("data_zip_(\\d+)")
        }
    }

    lateinit var zipMeasurements: String

    override fun create(context: Context): MeasurementsDB {
        Log.i(javaClass.simpleName, "Started Init")
        ZipState.reset()
        val db = MeasurementsDB.getInstance(context)
        if (!HAS_EQ) {
            return db
        }
        val inputs = getInputStreams(context)
        Log.i(javaClass.simpleName, "Created Files")
        SequenceInputStream(Collections.enumeration(inputs)).use seq@{ seq: SequenceInputStream ->
            ZipInputStream(seq).use {
                val entry = it.nextEntry
                Log.i(javaClass.simpleName, "Zip Entry ${entry.name}")
                if (isZipCommitDBCommit(it, db)) {
                    Log.i(javaClass.simpleName, "Zip Commit ${ZipState.zipCommit}")
                    return@seq
                }
                Log.i(javaClass.simpleName, "Need To Rewrite Data")
                it.closeEntry()
                it.nextEntry
                val baos = ByteArrayOutputStream()
                val bytes = ByteArray(1 shl 20)
                while (true) {
                    val read = it.read(bytes)
                    if (read != -1) {
                        baos.write(bytes, 0, read)
                    } else {
                        break
                    }
                }
                Log.i(javaClass.simpleName, "Start Rewrite Data")
                putData(db, baos, context)
                runBlocking {
                    with(ComponentName(context, AltPhoneService::class.java)) {
                        if (this in context.getSystemService<ActivityManager>()!!
                                .getRunningServices(Int.MAX_VALUE)
                                .map(ActivityManager.RunningServiceInfo::service)
                        ) {
                            val intent = Intent().setComponent(this@with)
                            val connection = object : ServiceConnection {
                                override fun onServiceConnected(
                                    name: ComponentName?,
                                    service: IBinder?
                                ) {
                                    val binder = IAltPhoneServiceInterface.Stub.asInterface(service)
                                    binder.stop()
                                    context.stopService(intent)
                                    context.unbindService(this)
                                }

                                override fun onServiceDisconnected(name: ComponentName?) {}

                                override fun onBindingDied(name: ComponentName?) =
                                    super.onBindingDied(name)

                                override fun onNullBinding(name: ComponentName?) =
                                    super.onNullBinding(name)
                            }
                            context.bindService(intent, connection, Service.BIND_DEBUG_UNBIND)
                        }
                    }
                    SingleSettingsManager.getSettings(context)
                        .updateData { settings ->
                            settings.toBuilder()
                                .setInit(true)
                                .setReset(settings.init)
                                .clearHeadphone()
                                .build()
                        }
                    AlternateSettingsManager.getSettings(context)
                        .updateData { settings ->
                            settings.toBuilder()
                                .setInit(true)
                                .setReset(settings.init)
                                .clearSource()
                                .clearTarget()
                                .build()
                        }
                }
                Log.i(javaClass.simpleName, "End Rewrite Data")
            }
        }
        inputs.forEach(InputStream::close)
        return db
    }

    private fun getInputStreams(context: Context): List<InputStream> =
        listOf(R.raw::class.java.fields, R.raw::class.java.declaredFields)
            .flatMap(Array<Field>::toList)
            .distinctBy(Field::getName)
            .filter { dataZipPattern.matcher(it.name).find() }
            .sortedBy {
                val matcher = dataZipPattern.matcher(it.name)
                matcher.find()
                return@sortedBy matcher.group(1)!!.toInt()
            }
            .map { it.getInt(null) }
            .map {
                val stream = context.resources!!.openRawResource(it)
                Log.v(javaClass.simpleName, stream.toString())
                return@map stream
            }
            .toList()

    private fun isZipCommitDBCommit(zipIn: ZipInputStream, db: MeasurementsDB): Boolean {
        val baos = ByteArrayOutputStream()
        while (true) {
            val bytes = ByteArray(40)
            val read = zipIn.read(bytes)
            if (read != -1) {
                baos.write(bytes, 0, read)
            } else {
                break
            }
        }
        baos.close()
        ZipState.zipCommit = baos.toString()
        ZipState.shouldSkipMeasurements = db.initDao().getHash() ?: "" == ZipState.zipCommit
        return ZipState.shouldSkipMeasurements
    }

    override fun dependencies(): List<Class<out Initializer<*>>> = emptyList()

    private fun putData(db: MeasurementsDB, baos: ByteArrayOutputStream, context: Context) =
        db.runInTransaction {
            Log.i(javaClass.simpleName, "Clearing Data")
            db.headphoneDao().deleteAll()
            db.measurementDao().deleteAll()
            Log.i(javaClass.simpleName, "Cleared Data")
            baos.toString().trim()
                .lines()
                .forEach {
                    val headphone = Headphone(it)
                    Log.v(javaClass.simpleName, "Inserted $headphone")
                    val id = db.headphoneDao().insertHeadphone(headphone)
                    db.measurementDao()
                        .insertAll(Measurement.from(headphone.measurements!!, id.toInt()))
                    Log.v(javaClass.simpleName, "Inserted values for ${headphone.name}")
                }
            db.initDao().setCommitHash(Init(ZipState.zipCommit))
            Log.v(javaClass.simpleName, "Updated data to ${ZipState.zipCommit}")
        }

    object ZipState {
        var zipCommit: String = ""
        var shouldSkipMeasurements: Boolean = false

        fun reset() {
            zipCommit = ""
            shouldSkipMeasurements = false;
        }
    }
}