package uk.ac.liverpool.altphone.headphone.adapters

import android.view.ViewGroup
import androidx.core.util.Consumer
import androidx.recyclerview.widget.ListAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.MeasurementsDB

class SingleHeadphoneAdapter(private val consumer: Consumer<Headphone>) :
    ListAdapter<Headphone, HeadphoneHolder>(DiffObject) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        HeadphoneHolder.makeHolder(parent)

    override fun onBindViewHolder(holder: HeadphoneHolder, position: Int) {
        val headphone = getItem(position)
        val targets by lazy {
            return@lazy MeasurementsDB.getInstance(holder.binding.root.context)
                .headphoneDao()
                .getAllByNameSingleNow(headphone.name!!)
        }
        val button = holder.binding.button
        button.text = headphone.name!!
        button.setOnClickListener {
            if (targets.size == 1) {
                consumer.accept(targets[0])
            } else {
                MaterialAlertDialogBuilder(holder.binding.root.context)
                    .setTitle("Select Target FR for ${headphone.name}")
                    .setCancelable(false)
                    .setNegativeButton("Dismiss") { di, _ ->
                        di.dismiss()
                    }
                    .setItems(
                        targets.map { "${it.target} by ${it.outlet}" }.toTypedArray()
                    ) { _, which ->
                        consumer.accept(targets[which])
                    }
                    .show()
            }

        }
    }
}