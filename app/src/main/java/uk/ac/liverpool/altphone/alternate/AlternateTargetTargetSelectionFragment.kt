package uk.ac.liverpool.altphone.alternate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.Consumer
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.databinding.ActivityHeadphoneBinding
import uk.ac.liverpool.altphone.headphone.adapters.DiffObject
import uk.ac.liverpool.altphone.headphone.adapters.HeadphoneHolder
import uk.ac.liverpool.altphone.measurements.BandsInfoManager
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.Measurement
import uk.ac.liverpool.altphone.measurements.MeasurementsDB
import uk.ac.liverpool.altphone.measurements.toInterpolator
import uk.ac.liverpool.altphone.measurements.toProto
import uk.ac.liverpool.altphone.protos.BandsInfo
import uk.ac.liverpool.altphone.utils.SimpleTextWatcher
import java.util.Comparator
import java.util.DoubleSummaryStatistics
import java.util.function.Function

class AlternateTargetTargetSelectionFragment : Fragment(), Consumer<Headphone> {

    val source: AlternateTargetTargetSelectionFragmentArgs by navArgs()
    private val bands: BandsInfo by lazy {
        runBlocking { BandsInfoManager.getSettings(requireContext()).data.first() }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.activity_headphone, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = ActivityHeadphoneBinding.bind(view)
        val recycler = binding.recycler
        val adapter: ListAdapter<Headphone, HeadphoneHolder> = AlternateTargetAdapter(this)
        recycler.adapter = adapter
        var data: LiveData<List<Headphone>>?
        data = MeasurementsDB.getInstance(requireContext())
            .headphoneDao()
            .getAllApplicableTargets(source.source.name!!, "")
        val observer = Observer<List<Headphone>>(adapter::submitList)
        data.observe(viewLifecycleOwner, observer)
        val layout = LinearLayoutManager(requireContext())
        recycler.layoutManager = layout
        recycler.addItemDecoration(DividerItemDecoration(requireContext(), layout.orientation))
        val text = binding.nameBox
        text.addTextChangedListener(object : SimpleTextWatcher() {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                data!!.removeObservers(viewLifecycleOwner)
                data = null
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                data = MeasurementsDB.getInstance(requireContext())
                    .headphoneDao()
                    .getAllApplicableTargets(source.source.name!!, text.text.toString())
                data!!.observe(viewLifecycleOwner, observer)
            }
        })
    }

    override fun accept(headphone: Headphone) {
        val best = getBestStats(headphone)
        val stats = best.stats
        val actual = with(bands.info) {
            maxLevel - minLevel
        }
        val range = stats.max - stats.min
        if (range <= actual) {
            runBlocking {
                AlternateSettingsManager.getSettings(requireContext())
                    .updateData {
                        it.toBuilder()
                            .setSource(best.source.toProto())
                            .setTarget(best.target.toProto())
                            .build()
                    }
            }
            findNavController().navigate(
                R.id.returnTarget,
                AlternateTargetFragmentArgs.Builder()
                    .setCheckTarget(true)
                    .setTarget(best.target)
                    .setCheckSource(true)
                    .setSource(best.source)
                    .build()
                    .toBundle()
            )
        } else {
            Snackbar.make(
                requireContext(),
                requireView(),
                "This selection is incompatible with this device",
                Snackbar.LENGTH_LONG
            )
                .setAction("Details") {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle("Details")
                        .setMessage(
                            "The profile to match the ${source.source.name} to the ${headphone.name} requires $range dB of range. This device only supports $actual dB."
                        )
                        .setNegativeButton("Dismiss") { di, _ -> di.dismiss() }
                        .setCancelable(true)
                        .show()
                }
                .show()
        }
    }

    private fun getBestStats(target: Headphone): AlternatePotSettings {
        var dao = MeasurementsDB.getInstance(requireContext())
            .headphoneDao()
        var sources = dao.getAllByNameNow(source.source.name!!)
        var targets = dao.getAllByNameNow(target.name!!)
        return sources.parallelStream()
            .flatMap { src ->
                return@flatMap targets.parallelStream()
                    .filter { tar ->
                        src.target == tar.target && src.outlet == tar.outlet
                    }
                    .map { tar ->
                        src to tar
                    }
            }
            .distinct()
            .map { AlternatePotSettings(it.first, it.second, getMeasurementStats(it.first, it.second)) }
            .min(Comparator.comparing(Function<AlternatePotSettings, DoubleSummaryStatistics> { t -> t.stats }, Comparator.comparingDouble(DoubleSummaryStatistics::getAverage)))
            .get()
    }

    private fun getMeasurementStats(source: Headphone, headphone: Headphone): DoubleSummaryStatistics {
        val interpolator = with(
            MeasurementsDB.getInstance(requireContext())
                .measurementDao()
        ) {
            val sourceMap = getAllByHeadphone(source.id!!)
                .associate { it.frequency to it.level }
            getAllByHeadphone(source.id!!)
                .associate { it.frequency to it.level }.mapValues {
                    sourceMap[it.key]!! - it.value
                }
                .toList()
                .map {
                    Measurement(
                        intArrayOf(source.id!!, headphone.id!!).contentHashCode(),
                        it.first,
                        it.second
                    )
                }
                .toInterpolator()
        }
        interpolator.keys
            .map { it to interpolator[it] }
        return interpolator.getBandFreqs(bands)
            .stream()
            .mapToDouble(Double::toDouble)
            .summaryStatistics()
    }
}

data class AlternatePotSettings(val source: Headphone, val target: Headphone, val stats : DoubleSummaryStatistics)

class AlternateTargetAdapter(val consumer: Consumer<Headphone>) :
    ListAdapter<Headphone, HeadphoneHolder>(DiffObject) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeadphoneHolder =
        HeadphoneHolder.makeHolder(parent)

    override fun onBindViewHolder(holder: HeadphoneHolder, position: Int) {
        val headphone = getItem(position)
        val button = holder.binding.button
        button.text = headphone.name!!
        button.setOnClickListener {
            consumer.accept(headphone)
        }
    }
}