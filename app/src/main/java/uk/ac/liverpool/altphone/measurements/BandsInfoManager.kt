package uk.ac.liverpool.altphone.measurements

import android.content.Context
import android.util.Log
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.createDataStore
import java.io.InputStream
import java.io.OutputStream
import uk.ac.liverpool.altphone.protos.BandsInfo

object BandsInfoManager : Serializer<BandsInfo> {

    private var settings: DataStore<BandsInfo>? = null

    override val defaultValue: BandsInfo = BandsInfo.getDefaultInstance()

    override fun readFrom(input: InputStream): BandsInfo = BandsInfo.parseFrom(input)

    override fun writeTo(t: BandsInfo, output: OutputStream) = t.writeTo(output)

    private fun handleCorruption(ex: CorruptionException): BandsInfo {
        Log.e(BandsInfoManager::javaClass.name, ex.toString(), ex)
        return defaultValue
    }

    fun getSettings(context: Context): DataStore<BandsInfo> = settings ?: synchronized(this) {
        settings ?: context.createDataStore(
            fileName = "bands_info.pb",
            serializer = BandsInfoManager,
            corruptionHandler = ReplaceFileCorruptionHandler(BandsInfoManager::handleCorruption)
        )
            .also { settings = it }

    }

}