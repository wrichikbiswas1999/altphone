package uk.ac.liverpool.altphone.sessions

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.audiofx.AudioEffect
import java.time.Instant

class SessionOpenedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val now = Instant.now()
        val sessionId = intent.getIntExtra(AudioEffect.EXTRA_AUDIO_SESSION, -1)
        val name = intent.getStringExtra(AudioEffect.EXTRA_PACKAGE_NAME)
        with(Session(sessionId, name, now, null, true)) {
            SessionsDB.getInstance(context).sessionDao()
                .putSession(this)
        }
    }
}