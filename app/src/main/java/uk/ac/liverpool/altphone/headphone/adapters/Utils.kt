package uk.ac.liverpool.altphone.headphone.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.getSystemService
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.databinding.ButtonViewBinding
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.MeasurementsDB

object DiffObject : DiffUtil.ItemCallback<Headphone>() {
    override fun areItemsTheSame(oldItem: Headphone, newItem: Headphone): Boolean =
        oldItem.id == newItem.id


    override fun areContentsTheSame(oldItem: Headphone, newItem: Headphone): Boolean =
        oldItem == newItem

}

class HeadphoneHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
    val binding: ButtonViewBinding = ButtonViewBinding.bind(view)

    companion object {
        fun makeHolder(parent: ViewGroup): HeadphoneHolder = HeadphoneHolder(
            parent.context.getSystemService<LayoutInflater>()!!
                .inflate(R.layout.button_view, parent, false)
        )
    }
}

public fun Headphone.makeMessage() : String = "Name: ${name}\nOutlet: ${outlet}\nTarget: ${target}\nMeasurements: $measurements"

public fun Headphone.makeArray(context: Context): Array<String> =
    MeasurementsDB.getInstance(context)
        .measurementDao()
        .getAllByHeadphone(id!!)
        .sortedBy { it.frequency }
        .map { "${it.frequency} Hz: ${it.level} dB" }
        .toTypedArray()