package uk.ac.liverpool.altphone.sessions

import android.content.Context
import android.util.Log
import androidx.startup.Initializer
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.utils.HAS_EQ

class SessionsDBInitializer : Initializer<SessionsDB> {

    override fun create(context: Context): SessionsDB = with(context) {
        val db = SessionsDB.getInstance(context)
        if (HAS_EQ) {
            LegacySessionSettingsManager.run {
                val need = needForceLegacy(this@with)
                Log.i(this::class.java.name, "Setting Force: $need, Can: ${true}")
                runBlocking {
                    with(getSettings(context).data.first()) {
                        if (!init) {
                            setLegacyMode(context, need)
                            getSettings(context)
                                .updateData {
                                    it.toBuilder().setLegacyAvailable(true).setInit(true).build()
                                }
                        }
                    }
                }
            }
        }
        return db
    }

    override fun dependencies(): List<Class<out Initializer<*>>> = emptyList()
}