package uk.ac.liverpool.altphone.headphone

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import uk.ac.liverpool.altphone.databinding.ActivityHeadphoneBinding
import uk.ac.liverpool.altphone.headphone.adapters.HeadphoneHolder
import uk.ac.liverpool.altphone.headphone.adapters.HeadphoneViewerAdapter
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.MeasurementsDB
import uk.ac.liverpool.altphone.utils.SimpleTextWatcher

class HeadphoneActivity : AppCompatActivity() {

    companion object {
        val ACTION_SINGLE_HEADPHONE = javaClass.`package`!!.name + ".action.SINGLE_HEADPHONE"
        val EXTRA_HEADPHONE = javaClass.`package`!!.name + ".extra.HEADPHONE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityHeadphoneBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val recycler = binding.recycler
        val adapter: ListAdapter<Headphone, HeadphoneHolder> = when(intent.action) {
            else -> HeadphoneViewerAdapter(this)
        }
        recycler.adapter = adapter
        var data: LiveData<List<Headphone>>?
        data = MeasurementsDB.getInstance(this)
            .headphoneDao()
            .getAllByText("")
        val observer = Observer<List<Headphone>>(adapter::submitList)
        data.observe(this, observer)
        val layout = LinearLayoutManager(this)
        recycler.layoutManager = layout
        recycler.addItemDecoration(DividerItemDecoration(this, layout.orientation))
        val text = binding.nameBox
        text.addTextChangedListener(object : SimpleTextWatcher() {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                data!!.removeObservers(this@HeadphoneActivity)
                data = null
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                with(text.text.toString()) {
                    data = MeasurementsDB.getInstance(this@HeadphoneActivity)
                        .headphoneDao()
                        .getAllByText(this)
                }
                data!!.observe(this@HeadphoneActivity, observer)
            }
        })
    }

    fun finishWithHeadphone(headphone: Headphone) {
        setResult(Activity.RESULT_OK, Intent().putExtra(EXTRA_HEADPHONE, ByteArrayOutputStream().apply {
            use { baos ->
                ObjectOutputStream(baos).use {
                    it.writeObject(headphone)
                }
            }
        }.toByteArray()))
        finishAndRemoveTask()
    }
}