package uk.ac.liverpool.altphone.utils

import android.media.audiofx.AudioEffect
import android.media.audiofx.Equalizer
import android.util.Log
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import java.util.Objects

val HAS_EQ: Boolean by lazy {
    AudioEffect.EFFECT_TYPE_EQUALIZER in  arrayOf(AudioEffect.queryEffects(), Equalizer.queryEffects())
        .flatten()
        .map(AudioEffect.Descriptor::type)
}


fun <T> T?.ifNotNull(runnable: T.() -> Unit) {
    if (Objects.nonNull(this)) {
        this!!.runnable()
    }
}

fun <T, R> T?.mapIfNotNull(runnable: T.() -> R): R? = if (Objects.nonNull(this)) {
    this!!.runnable()
} else {
    null
}


fun <T : Serializable> T?.serialize(): ByteArray = this.mapIfNotNull {
    with(ByteArrayOutputStream()) {
        use {
            ObjectOutputStream(this).use {
                it.writeObject(this@mapIfNotNull)
            }
        }
        return@mapIfNotNull toByteArray()
    }
} ?: ByteArray(0)

fun <R : Serializable> ByteArray.deserialize(): R? {
    try {
        return this.mapIfNotNull {
            with(ByteArrayInputStream(this)) {
                use {
                    ObjectInputStream(this)
                        .use {
                            return@mapIfNotNull it.readObject()
                        }
                }
            }
        } as R?
    } catch (e: Exception) {
        Log.e("Deserialization", e.toString(), e)
        return null
    }
}