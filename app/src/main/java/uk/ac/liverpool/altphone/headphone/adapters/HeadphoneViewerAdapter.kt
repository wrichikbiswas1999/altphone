package uk.ac.liverpool.altphone.headphone.adapters

import android.content.Context
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.ListAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.headphone.HeadphoneActivity
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.MeasurementsDB

class HeadphoneViewerAdapter(activity: HeadphoneActivity) :
    ListAdapter<Headphone, HeadphoneHolder>(DiffObject) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = HeadphoneHolder.makeHolder(parent)

    override fun onBindViewHolder(holder: HeadphoneHolder, position: Int) {
        val button = holder.itemView.findViewById<AppCompatButton>(R.id.button)
        button.text = getItem(position).name
        button.setOnClickListener {
            MaterialAlertDialogBuilder(button.context)
                .setTitle(button.text)
                .setMessage(getItem(position)!!.makeMessage())
                .setCancelable(false)
                .setNegativeButton("Dismiss") { dialog, _ -> dialog.dismiss() }
                .setNeutralButton("Measurements") { _, _ ->
                    MaterialAlertDialogBuilder(button.context)
                        .setTitle(button.text)
                        .setItems(makeArray(getItem(position), button.context), null)
                        .setCancelable(false)
                        .setPositiveButton("Dismiss") { measurementDialog, _ ->
                            measurementDialog.dismiss()
                        }
                        .show()
                }.show()
        }
    }


    private fun makeArray(headphone: Headphone, context: Context): Array<String> =
        MeasurementsDB.getInstance(context)
            .measurementDao()
            .getAllByHeadphone(headphone.id!!)
            .sortedBy { it.frequency }
            .map { "${it.frequency} Hz: ${it.level} dB" }
            .toTypedArray()


}
