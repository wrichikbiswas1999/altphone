package uk.ac.liverpool.altphone.headphone

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import java.io.ByteArrayInputStream
import java.io.ObjectInputStream
import java.util.Objects
import uk.ac.liverpool.altphone.measurements.Headphone

class SingleHeadphoneContract : ActivityResultContract<Void, Headphone?>() {

    override fun parseResult(resultCode: Int, intent: Intent?): Headphone? {
        if (resultCode == Activity.RESULT_OK && Objects.nonNull(intent)) {
            ByteArrayInputStream(intent!!.getByteArrayExtra(HeadphoneActivity.EXTRA_HEADPHONE))
                .use { baos ->
                    ObjectInputStream(baos).use {
                        return@parseResult it.readObject() as Headphone
                    }
                }
        }
        return null
    }

    override fun createIntent(context: Context, input: Void): Intent = Intent(HeadphoneActivity.ACTION_SINGLE_HEADPHONE)
        .setClass(context, HeadphoneActivity::class.java)

}