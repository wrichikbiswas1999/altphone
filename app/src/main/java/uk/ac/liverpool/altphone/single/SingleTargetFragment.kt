package uk.ac.liverpool.altphone.single

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.Consumer
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.createDataStore
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import uk.ac.liverpool.altphone.R
import uk.ac.liverpool.altphone.databinding.ActivityHeadphoneBinding
import uk.ac.liverpool.altphone.databinding.LayoutFragmentSingleTargetSourceBinding
import uk.ac.liverpool.altphone.headphone.adapters.HeadphoneHolder
import uk.ac.liverpool.altphone.headphone.adapters.SingleHeadphoneAdapter
import uk.ac.liverpool.altphone.measurements.BandsInfoManager
import uk.ac.liverpool.altphone.measurements.Headphone
import uk.ac.liverpool.altphone.measurements.MeasurementsDB
import uk.ac.liverpool.altphone.measurements.toInterpolator
import uk.ac.liverpool.altphone.measurements.toProto
import uk.ac.liverpool.altphone.measurements.toRoom
import uk.ac.liverpool.altphone.protos.BandsInfo
import uk.ac.liverpool.altphone.protos.SingleSettings
import uk.ac.liverpool.altphone.utils.SimpleTextWatcher
import uk.ac.liverpool.altphone.utils.ifNotNull
import java.io.InputStream
import java.io.OutputStream
import java.util.DoubleSummaryStatistics

class SingleTargetSourceFragment : Fragment() {

    private val args: Headphone? by lazy {
        val working = navArgs<SingleTargetSourceFragmentArgs>().value
        if (working.checkHeadphone) {
            working.headphone
        } else {
            runBlocking {
                with(SingleSettingsManager.getSettings(requireContext()).data.first()) {
                    if (hasHeadphone()) {
                        this.headphone.toRoom()
                    } else {
                        null
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.layout_fragment_single_target_source, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = LayoutFragmentSingleTargetSourceBinding.bind(view)
        binding.button.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.openSingleSelector))
        args.ifNotNull {
            binding.nameBox.text = name
            binding.targetBox.text = "$target by $outlet"
        }
    }
}

class SingleTargetSelectionFragment : Fragment(), Consumer<Headphone> {

    private val bands: BandsInfo by lazy {
        runBlocking { BandsInfoManager.getSettings(requireContext()).data.first() }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.activity_headphone, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = ActivityHeadphoneBinding.bind(view)
        val recycler = binding.recycler
        val adapter: ListAdapter<Headphone, HeadphoneHolder> = SingleHeadphoneAdapter(this)
        recycler.adapter = adapter
        var data: LiveData<List<Headphone>>? = MeasurementsDB.getInstance(requireContext())
            .headphoneDao()
            .searchAllByName("")
        val observer = Observer<List<Headphone>>(adapter::submitList)
        data!!.observe(viewLifecycleOwner, observer)
        val layout = LinearLayoutManager(requireContext())
        recycler.layoutManager = layout
        recycler.addItemDecoration(DividerItemDecoration(requireContext(), layout.orientation))
        val text = binding.nameBox
        text.addTextChangedListener(object : SimpleTextWatcher() {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                data!!.removeObservers(viewLifecycleOwner)
                data = null
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                data = MeasurementsDB.getInstance(requireContext())
                    .headphoneDao()
                    .searchAllByName(text.text.toString())
                data!!.observe(viewLifecycleOwner, observer)
            }
        })
    }

    override fun accept(headphone: Headphone) {
        val stats = getMeasurementStats(headphone)
        val actual = with(bands.info) {
            maxLevel - minLevel
        }
        val range = stats.max - stats.min
        if (range <= actual) {
            runBlocking {
                SingleSettingsManager.getSettings(requireContext())
                    .updateData {
                        it.toBuilder()
                            .clearReset()
                            .clearHeadphone()
                            .setHeadphone(headphone.toProto())
                            .build()
                    }
            }
            findNavController().navigate(
                R.id.returnHeadphone,
                SingleTargetSourceFragmentArgs.Builder().setCheckHeadphone(true)
                    .setHeadphone(headphone)
                    .build().toBundle()
            )
        } else {
            Snackbar.make(
                requireContext(),
                requireView(),
                "This selection is incompatible with this device",
                Snackbar.LENGTH_LONG
            )
                .setAction("Details") {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle("Details")
                        .setMessage(
                            "The profile to match the ${headphone.name} to the ${headphone.target} curve by ${headphone.target} requires $range dB of range. This device only supports $actual dB."
                        )
                        .setNegativeButton("Dismiss") { di, _ -> di.dismiss() }
                        .setCancelable(true)
                        .show()
                }
                .show()
        }
    }

    private fun getMeasurementStats(headphone: Headphone): DoubleSummaryStatistics {
        val interpolator = MeasurementsDB.getInstance(requireContext())
            .measurementDao()
            .getAllByHeadphone(headphone.id!!)
            .toInterpolator()
        interpolator.keys
            .map { it to interpolator[it] }
        return bands.info
            .bandsList
            .map { band ->
                val arr = doubleArrayOf(band.minWork, band.maxWork)
                val keys = interpolator
                    .keys
                    .toMutableList()
                    .apply {
                        add(arr[0])
                        add(arr[1])
                    }
                    .filter { it >= arr[0] }
                    .filter { it <= arr[1] }
                    .sorted()
                    .distinct()
                    .toDoubleArray()
                (0 until (keys.size - 1))
                    .map { ind ->
                        val height = keys[ind + 1] - keys[ind]
                        val base =
                            (interpolator.getMeasurementFor(keys[ind]) + interpolator.getMeasurementFor(
                                keys[ind + 1]
                            )) / 2
                        base * height
                    }.sum() / (arr[1] - arr[0])
            }
            .stream()
            .mapToDouble(Double::toDouble)
            .summaryStatistics()
    }
}



object SingleSettingsManager : Serializer<SingleSettings> {

    private var settings: DataStore<SingleSettings>? = null

    override val defaultValue: SingleSettings = SingleSettings.getDefaultInstance()

    override fun readFrom(input: InputStream): SingleSettings = SingleSettings.parseFrom(input)

    override fun writeTo(t: SingleSettings, output: OutputStream) = t.writeTo(output)

    private fun handleCorruption(ex: CorruptionException): SingleSettings {
        Log.e(SingleSettingsManager::javaClass.name, ex.toString(), ex)
        return SingleSettings.newBuilder()
            .setInit(true)
            .setReset(false)
            .build()
    }

    fun getSettings(context: Context): DataStore<SingleSettings> = settings ?: synchronized(this) {
        settings ?: context.createDataStore(
            fileName = "single_settings.pb",
            serializer = SingleSettingsManager,
            corruptionHandler = ReplaceFileCorruptionHandler(SingleSettingsManager::handleCorruption)
        )
            .also { settings = it }

    }

}