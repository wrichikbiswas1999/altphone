// IAltPhoneServiceInterface.aidl
package uk.ac.liverpool.altphone.service;

// Declare any non-default types here with import statements

interface IAltPhoneServiceInterface {

    String setMode(in String mode);

    String updateSettings();

    oneway void stop();

    String getMode();

    String getBands();
}