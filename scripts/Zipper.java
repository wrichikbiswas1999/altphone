import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zipper {
	
	public static void main(String[] args) throws IOException {
		initSubs();
		pullSubs();
		splitZip();
	}
	
	private static void initSubs() throws IOException {
		var builder = new ProcessBuilder("git", "submodule", "update", "--init", "--remote", "--recursive", "--force")
				.inheritIO();
		var process = builder.start();
		while (process.isAlive()) {
			try {
				int exit = process.waitFor();
				if (exit != 0) {
					System.exit(exit);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void pullSubs() throws IOException {
		var builder = new ProcessBuilder("git", "submodule", "update", "--remote", "--recursive")
				.inheritIO();
		var process = builder.start();
		while (process.isAlive()) {
			try {
				int exit = process.waitFor();
				if (exit != 0) {
					System.exit(exit);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void splitZip() throws IOException {
		var buffer = ByteBuffer.allocate((int) Math.pow(2, 20));
		var dir = Path.of("app", "src", "main", "res", "raw");
		try (var bais = new ByteArrayInputStream(makeZip().toByteArray());
			 var channel = Channels.newChannel(bais);
			 var dirStream = Files.newDirectoryStream(dir);
			 var stream = StreamSupport.stream(dirStream.spliterator(), true)) {
			stream.parallel()
					.forEach(path -> {
						do {
							try {
								Files.deleteIfExists(path);
							} catch (IOException e) {
								e.printStackTrace();
							}
						} while (Files.exists(path));
					});
			int i = 0;
			do {
				do {
					channel.read(buffer);
				} while (buffer.hasRemaining() && (bais.available() > 0));
				var file = FileChannel.open(dir.resolve("data_zip_" + i), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
				buffer.flip();
				while (buffer.hasRemaining()) {
					file.write(buffer);
				}
				file.close();
				buffer.flip();
				i++;
			} while (bais.available() > 0);
		}
	}
	
	private static ByteArrayOutputStream makeZip() throws IOException {
		var out = new ByteArrayOutputStream();
		try (out; var zipOut = new ZipOutputStream(out);
			 var dataIn = makeMeasurements()) {
			zipOut.setLevel(Deflater.BEST_COMPRESSION);
			zipOut.putNextEntry(new ZipEntry("commit"));
			zipOut.write(commit().getBytes());
			zipOut.closeEntry();
			var entry = new ZipEntry("-");
			entry.setComment("AltPhone GraphicEQ Data");
			zipOut.putNextEntry(entry);
			zipOut.write(dataIn.toByteArray());
			zipOut.closeEntry();
		}
		return out;
	}
	
	private static ByteArrayOutputStream makeMeasurements() throws IOException {
		var out = new ByteArrayOutputStream();
		var results = Path.of("AutoEq", "results");
		Files.walk(results)
				.filter(path -> path.toString().endsWith("GraphicEQ.txt"))
				.flatMap(path -> {
					try {
						var rel = results.relativize(path);
						return Stream.of(String.format("\"%s\",\"%s\",\"%s\",\"%s\"\n", rel.getName(2), rel.getName(0), rel.getName(1), Files.readString(path).trim()));
					} catch (Exception e) {
						e.printStackTrace();
						return Stream.empty();
					}
				})
				.sorted()
				.map(String::getBytes)
				.forEach(out::writeBytes);
		out.close();
		return out;
	}
	
	private static String commit() throws IOException {
		var process = new ProcessBuilder("git", "submodule", "status")
				.start();
		do {
			try {
				process.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while (process.isAlive());
		return new String(process.getInputStream().readAllBytes()).trim()
				.lines()
				.filter(line -> line.contains("AutoEq"))
				.findAny()
				.get()
				.substring(1)
				.split(" ")[0];
		
	}
}
